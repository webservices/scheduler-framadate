FROM php:5-apache

COPY config/php.ini /usr/local/etc/php/
COPY src/ /var/www/scheduler
COPY config/simplesamlphp-1.14.14 /var/simplesamlphp

RUN apt-get -y update &&\
    apt-get -y upgrade &&\
    apt-get install -y --force-yes git libicu-dev php5-mysql php5-xdebug zip unzip zlib1g-dev php5-mcrypt libmcrypt-dev php5-gmp libgmp-dev php5-ldap &&\
    apt-get clean &&\
    docker-php-ext-install -j$(nproc) intl &&\
    docker-php-ext-install -j$(nproc) zip &&\
    docker-php-ext-install -j$(nproc) pdo_mysql &&\
    docker-php-ext-configure mcrypt &&\
    docker-php-ext-install -j$(nproc) mcrypt &&\
	ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h &&\
    docker-php-ext-configure gmp --with-gmp=/usr/include/x86_64-linux-gnu &&\
    docker-php-ext-install -j$(nproc) gmp &&\
    useradd scheduler &&\
    chown -R scheduler:scheduler /var/www/scheduler
RUN cd /var/www/scheduler &&\
    php -r "readfile('https://getcomposer.org/installer');" | php &&\
    ./composer.phar install &&\
    chown -R scheduler:scheduler vendor &&\
    cd -
RUN cd /var/simplesamlphp &&\
    php -r "readfile('https://getcomposer.org/installer');" | php &&\
    ./composer.phar install &&\
    chown -R scheduler:scheduler vendor &&\
    cd -
RUN a2enmod php5
RUN a2enmod rewrite
RUN sed -i "s/\b80\b/8080/g" /etc/apache2/ports.conf
RUN sed -i "s/\b443\b/18443/g" /etc/apache2/ports.conf

RUN chown -R scheduler:scheduler /var/www/scheduler

#RUN sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/5.6/apache2/php.ini
#RUN sed -i "s/error_reporting = .*$/error_reporting = E_ERROR | E_WARNING | E_PARSE/" /etc/php/5.6/apache2/php.ini

ENV APACHE_RUN_USER scheduler
ENV APACHE_RUN_GROUP scheduler
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

RUN chmod -R a+rw /var/log/ /var/lock/ /var/run/ /var/www/scheduler /var/simplesamlphp

ADD config/apache-config.conf /etc/apache2/sites-enabled/000-default.conf

EXPOSE 8080
#EXPOSE 18443
#CMD /bin/bash
CMD /usr/sbin/apache2ctl -D FOREGROUND
