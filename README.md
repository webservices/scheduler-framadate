# How to launch Cern Scheduler

## Launch docker container
```bash
> docker build -t scheduler_image .
> docker run --name scheduler -p 8080:80 -tdi scheduler_image
```

## Debugging
```bash
> docker exec -ti scheduler bash
> docker logs -f scheduler
```
